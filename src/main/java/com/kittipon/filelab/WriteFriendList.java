/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.filelab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kitti
 */
public class WriteFriendList {

    public static void main(String[] args) {
        LinkedList<Friend> friendList = new LinkedList<>();
        friendList.add(new Friend("Natthakritta", 21, "0929399331"));
        friendList.add(new Friend("Satit", 20, "0929324231"));
        friendList.add(new Friend("Siwakorn", 20, "0929334891"));

        FileOutputStream fos = null;
        try {
            File file = new File("list_of_friend.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(friendList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
